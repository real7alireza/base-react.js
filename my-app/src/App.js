import React , {Component} from 'react';
import logo from './logo.svg';
import './App.css';
import Person from './Person/Person'




// class App extends Component {
//   render(){
//     return(
//       <div className='App'>
//         <h1>hello I'm React App!!</h1>
//       </div>
//     )
//     // return React.createElement('div', {className:'App'}, React.createElement('h1', null, 'hello I\'m React App!!'))
//   }
// }

/************************************************************** */


// function App() {
//   return (
//     <div className="App">
//       <Person name='Alireza' age='22' />
//       <Person name='Max' age='28' />
//     </div>
//   );
// }


/************************************************************** */


// function App() {
//   return (
//     <div className="App">
//       <Person name='Max' age='30'/>
//       <Person name='Alireza' age='22'>My Hobbit : studing</Person>
//       <Person name='Hossein' age='15'/>
//     </div>
//   );
// }



/************************************************************** */


class App extends Component {
  
  state = {
    persons : [
      {name : 'Max', age : 30},
      {name : 'Alireza', age : 22},
      {name : 'Hossein', age : 15},
    ],
    otherState: 'some other state'
  }

  switchNameHandler = (newName) => {
    // console.log('Was clicked!')
    // DON'T DO THIS : this.state.persons[0].name = 'Maximilian'
    
    this.setState({
      persons : [
        {name : newName, age : 30},
        {name : 'Alireza', age : 22},
        {name : 'Hossein', age : 18}  
      ]
    })
  }

  nameChangedHandler = (event) => {
    this.setState({
      persons : [
        {name : 'Max', age: 30},
        {name : event.target.value, age: (event.target.value == 'Faezeh')?20:23},
        {name : 'Sarah', age: 26}
      ]
    })
  }

  render(){
    const style = {
      backgroundColor : 'white',
      font : 'inherit',
      border : '1px solid blue',
      padding : '8px',
      cursor : 'pointer'
    }
    return(
      <div className="App">
        <h1>Hi, I'm React App</h1>
        <p>This is really working</p>
        <button 
          style = {style}
          onClick={this.switchNameHandler.bind(this , 'Maximilian')}>Switch Name</button>
        {/* <button onClick={() => this.switchNameHandler('Maximilian!!!')}>Switch Name</button> */}
        {/* تابع بدون آرگومان را بدون پرانتر میزاریم
        چون اگر پرانتز بزاریم هنگام رندر کردن اجرا میکند
        درحالیکه ما میخواهیم پس از کلیک کردن این تابع اجرا شود
        
        تابع با آرگومان : نیاز دارد دیتایی را پاس بده
        روش اول :
        استفاده میکنیم bind در اینجا از
        که بدون اجرا کردن تابع دیتا را بهش پاس میده
        */}
        <Person 
          name={this.state.persons[0].name} 
          age={this.state.persons[0].age}/>
        <Person 
          name={this.state.persons[1].name} 
          age={this.state.persons[1].age}
          // روش دوم :
          click={() => this.switchNameHandler('Max!')}
          changed = {this.nameChangedHandler}
          >My Hobbit : Hard working!!!</Person>
        <Person 
          name={this.state.persons[2].name} 
          age={this.state.persons[2].age}/>
      </div>
    )
  }
}

export default App;

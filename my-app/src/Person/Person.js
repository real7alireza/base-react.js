import './Person.css'


const person = (props) => {
    // return <p id='ali'>I'm a Person and I am {Math.floor(Math.random()*30+1)} years old!!</p>
    return (
        <div className='People'>
            <p onClick={props.click}>I'm {props.name} and I am {props.age} years old!!</p>
            <p>{props.children}</p>    
            <input type='text' onChange={props.changed} />
        </div>
    )
}




// function Person(props) {
//     return(
//         <div  className='person'>
//             <h1>{props.name}</h1>
//             <p>Your age is : {props.age}</p>
//         </div>
//     )
// }

// export default Person
export default person